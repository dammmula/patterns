export default class Commentator {

    constructor() {
        this.startText = 'Доброго дня! Сьогодні з Вами я, Ibn Rushd, і я буду коментувати дане дійство. Що ж, сподіваюсь, усі готові, тому що видовище от-от почеться. Учасникам бажаю щасливої гри, і нехай удача завжди буде на вашому боці!';
        this.randomPhrases = [
            '“Недостатньо тільки отримати знання; їх треба застосувати. Недостатньо тільки бажати; треба діяти.”, – Йоганн Вольфганг фон Гете.',
            '“Є тільки один спосіб уникнути критики: нічого не робіть, нічого не говоріть і будьте ніким. “, – Аристотель.',
            '“Людина, якою вам судилося стати – це тільки та людина, якою ви самі вирішите стати.”, – Ральф Уолдо Емерсон.',
            `“Ви ніколи не перетнете океан, якщо не наберетеся мужності втратити берег з виду.”, – Христофор Колумб.`,
            ' “Два найважливіших дні у твоєму житті: день, коли ти з’явився на світ, і день, коли ти зрозумів навіщо”, – Марк Твен.',
            '“Варто тільки повірити, що ви можете – і ви вже на півдороги до цілі”, – Теодор Рузвельт.',
            ' “Навчіться говорити “Я не знаю”, і це вже буде прогрес.”, – Мойсей Маймонід.',
            '“Щастя – це не щось готове. Щастя залежить тільки від ваших дій. “, – Далай Лама.',
            '“Успіх – це здатність крокувати від однієї невдачі до іншої, не втрачаючи ентузіазму.”, – Вінстон Черчилль.',
            '“Ваш добробут залежить від ваших власних рішень.” – Джон Д. Рокфеллер.',
            '“Невдача – це просто можливість почати знову, але вже більш мудро.”, – Генрі Форд.',
            '“Жодного разу не впасти – це не найбільша заслуга в життя. Головне кожен раз підніматися.”, – Нельсон Мандела.',
            '“Слово “криза”, написане китайською мовою, складається з двох ієрогліфів: один означає “небезпека”, інший – “можливість”.”, – Джон Кеннеді.',
            ' “Ніхто не зможе побудувати для Вас міст, на якому Ви повинні перетнути потік життя, ніхто, крім Вас самих.”, – Фрідріх Ніцше.'
        ];
        this.toldPhrasesCount = 0;
    }

    welcome() {
        return this.startText;
    }

    introduceUsers(users) {
        let comment;
        if (users.length === 1) {
            comment = `Сьогодні в нас в гонці бере участь лише один учасник і це неперевершений ${users[0].name} на своєму Mercedes 230 SL небесно-блакитного кольору. Схоже, всі злякались його попередніх досягнень в клавогонках. Що ж, це й на краще, принаймні ми точно знаємо, що усі глядачі сьогодні вболівають саме за тебе, ${users[0].name}. Удачі і спробуй побити свій рекорд!`
        } else {
            comment = `Дозвольте представити сьогоднішніх учасників: ${users.map(({name}) => name).join(', ')}.
            Усі з них вже неодноразові чемпіони в цій сфері, тож битва буде запеклою.`
        }

        return comment;
    }

    tellCurrentStatus(room, secondsForGame) {
        let comment = '';
        const usersLeft = room.users.filter(({progressStatus}) => progressStatus < 100);
        const timePassed = this.getTimeDifference(room.startTime, Date.now());
        const timeLeft = Math.round(secondsForGame - timePassed);


        if (room.results.length) {
            comment = `Що ж, поки ви спостерігаєте за нашими гравцями, я вам нагадаю, хто вже фінішував: ${room.results.join(', ')},
            але це ще не все, гра досі триває.`
        }
        const firstUser = usersLeft.shift();
        let typedTextLength = firstUser.typedTextLength;

        comment += `Ситуація на даний момент дуже неоднозначна, тому тільки ваші вболівання і підтримка можуть допомогти! 
        До фініша наближається ${firstUser.name}, залишилось всього ${this.getTextDifference(firstUser.typedTextLength, room.textLength)} символи. `;

        for (let user of usersLeft) {
            comment += `За ним ${user.name}, який відстає від попереднього учасника лише на ${this.getTextDifference(typedTextLength, user.typedTextLength)} символи. `
        }
        comment += `На жаль, часу залишилось зовсім обмаль. ${timeLeft} секунд до кінця, поспішайте!`;
        return comment;

    }

    tellParticipantApproach(user) {
        const comment = `${user.name} наближається до фініша!`;
        return comment;
    }

    tellParticipantFinished(room, user) {
        const comment = `${user.name} уже на фініші! Пройшло всього ${this.getTimeDifference(room.startTime, user.finishTime)} секунди! Неймовірний результат! `
        return comment;
    }

    announceResults(room) {
        const {results, startTime} = room;
        let comment;

        if (results.length === 1) {
            comment = `Сьогодні ${results[0].name} перевершив самого себе. Запам'ятайте це ім'я і цю гонку, тому що обговорювати її будуть ще довго.
            А ви, наші глядачі, будете до кінця своїх днів радіти, що стали свідками такої величної події. Наш новий рекорд: ${this.getTimeDifference(startTime, results[0].finishTime)} секунди!
            Вражаюче, правда?`
        } else if (results.length === 2) {
            comment = `Обидва учасники виклались на всі 100, але все ж з невеликим відривом переміг ${results[0].name}. 
            Його час скаладає ${this.getTimeDifference(startTime, results[0].finishTime)} секунди. Чудова робота, так тримати!
            Відповідно на другому місці в нашій турнірній таблиці ${results[0].name}, який теж показав гарний результат: ${this.getTimeDifference(startTime, results[0].finishTime)} секунди.
            Є до чого прагнути, чекаємо на реванш!`
        } else {
            comment = `Ось і закінчився наш поєдинок. 
        Усі учасники були на висоті, але переможець в нас тільки один. Тож оголошую призерів.
        Отже, сьогоднішній чемпіон: ${results[0].name} з результатом в ${this.getTimeDifference(startTime, results[0].finishTime)} секунди.
        Наступним прийшов ${results[1].name}, і в нього це вийшло за ${this.getTimeDifference(startTime, results[1].finishTime)} секунди.
        А на третьому місці в нас ${results[2].name}, з невеликим відкривом в ${this.getTimeDifference(startTime, results[2].finishTime)} секунди. 
        Усі молодці, гарна була гра!`;
        }

        return comment;
    }

    tellRandomPhrase() {
        if (this.toldPhrasesCount === this.randomPhrases.length) {
            this.toldPhrasesCount = 0;
        }
        return this.randomPhrases[this.toldPhrasesCount++];
    }

    getTextDifference(text1, text2) {
        return Math.abs(text2 - text1);
    }

    getTimeDifference(start, end) {
        const diff = (end - start) / 1000;
        return Math.round(diff);
    }
}