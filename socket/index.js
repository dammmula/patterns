import * as config from "./config";
import {
  MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_BEFORE_RANDOM_PHRASE,
  SECONDS_FOR_GAME,
  SECONDS_INTERVAL_FOR_GAME_COMMENT,
  SECONDS_TIMER_BEFORE_START_GAME, SECONDS_TO_SHOW_COMMENT, SYMBOLS_LEFT_FOR_WARNING
} from "./config";
import {findUser, findRoom} from "./helper.js";
import Commentator from './commentator.js';


export const rooms = [];
export const users = new Set();
let activeRoom;
let commentator = new Commentator(activeRoom);

export default io => {
  io.on("connection", socket => {

    const username = socket.handshake.query.username;

    if (users.has(username)) {
      socket.emit("GAME_EVENT", {
        name: "ERROR",
        args: {
          error: "user",
          message: "User already exists"
        }
      });
    }


    socket.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: {rooms}});
    users.add(username);


    let intervalID
    let timeoutDeleteCommentID;
    let timeoutRandomPhraseID;

    const proxyHandler = {
      get(target, prop) {
        clearTimeout(timeoutDeleteCommentID);
        clearTimeout(timeoutRandomPhraseID);
        timeoutDeleteCommentID = setTimeout(() => {

          socket.emit("GAME_EVENT", {name: "DELETE_COMMENT"});
          socket.to(activeRoom.name).emit("GAME_EVENT", {name: "DELETE_COMMENT"});

          timeoutRandomPhraseID = setTimeout(() => {

            socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: target.tellRandomPhrase()}});
            socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: target.tellRandomPhrase()}});
          }, SECONDS_BEFORE_RANDOM_PHRASE * 1000);
        }, SECONDS_TO_SHOW_COMMENT * 1000);

        return target[prop];
      }
    }
    commentator = new Proxy(commentator, proxyHandler); //Proxy

    socket.on("ADD_ROOM", onAddRoom);
    socket.on("JOIN_ROOM", onJoinRoom);
    socket.on("USERS_READY", onUsersReady);
    socket.on("TIMER_DONE", onTimerDone);
    socket.on("UPDATE_ROOM", onUpdateRoom)
    socket.on("UPDATE_PROGRESS", onUpdateProgress);
    socket.on("END_GAME", onEndGame);
    socket.on("LEAVE_ROOM", onLeaveRoom);
    socket.on("disconnect", onDisconnect);

    function onAddRoom(name) {
      if (name && !name.trim().length) {
        socket.emit("GAME_EVENT", {
          name: "ERROR",
          args: {
            error: "room",
            message: "Invalid room name"
          }
        });
      } else if (findRoom(name, rooms)) {
        socket.emit("GAME_EVENT", {
          name: "ERROR",
          args: {
            error: "room",
            message: "Room already exists"
          }
        });
      } else {
        rooms.push({
          name,
          usersCount: 0,
          users: [],
          id: socket.id,
          results: [],
          startTime: null
        });

        socket.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: { rooms }});
        socket.broadcast.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: {rooms}});

        const room = findRoom(name, rooms);
        socket.emit("GAME_EVENT", {name: "ADD_ROOM_DONE", args: { room }});
      }
    }

    function onJoinRoom(room) {
      if (room.usersCount === MAXIMUM_USERS_FOR_ONE_ROOM) {
        socket.emit("GAME_EVENT", {
          name: "ERROR",
          args: {
              error: "maximumUsers",
              message: "This room is already full. Choose another one"
        }
      });
        return;
      }

      socket.join(room.name);
      activeRoom = findRoom(room.name, rooms);

      activeRoom.usersCount++;
      activeRoom.users.push({
        name: username,
        readyStatus: false,
        progressStatus: 0,
        typedTextLength: 0
      });

      socket.broadcast.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: {rooms}});
      socket.emit("GAME_EVENT", {name: "RENDER_ROOM", args: {room: activeRoom}});
      socket.to(room.name).emit("GAME_EVENT", {name: "RENDER_ROOM", args: {room: activeRoom}});

    }

    function onUsersReady() {
      socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.welcome()}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.welcome()}});
      socket.emit("GAME_EVENT", {name: "START_TIMER", args: {timeBeforeGame: SECONDS_TIMER_BEFORE_START_GAME}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "START_TIMER", args: {timeBeforeGame: SECONDS_TIMER_BEFORE_START_GAME}});
    }

    function onTimerDone(textLength) {
      activeRoom.startTime = Date.now();
      activeRoom.textLength = textLength;

      intervalID = setInterval(() => {
        socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellCurrentStatus(activeRoom, SECONDS_FOR_GAME)}});
        socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellCurrentStatus(activeRoom, SECONDS_FOR_GAME)}});
      }, SECONDS_INTERVAL_FOR_GAME_COMMENT * 1000)


      socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.introduceUsers(activeRoom.users)}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.introduceUsers(activeRoom.users)}});
      socket.emit("GAME_EVENT",  {name: "START_GAME" , args: {timeLeft: SECONDS_FOR_GAME}});
      socket.to(activeRoom.name).emit("GAME_EVENT",  {name: "START_GAME" , args: {timeLeft: SECONDS_FOR_GAME}});
    }

    function onUpdateRoom (readyStatus) {
      findUser(username, activeRoom.users).readyStatus = readyStatus;
      socket.emit("GAME_EVENT", {name: "RENDER_ROOM", args: {room: activeRoom}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "RENDER_ROOM", args: {room: activeRoom}});
    }

    function onUpdateProgress(typedTextLength) {
      const user = findUser(username, activeRoom.users);
      const progressPercent = Math.round(typedTextLength * 100 / activeRoom.textLength);

      user.typedTextLength = typedTextLength;
      user.progressStatus = progressPercent;
      const textLeft = activeRoom.textLength - typedTextLength;

      if (textLeft === SYMBOLS_LEFT_FOR_WARNING) {
        socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellParticipantApproach(user)}});
        socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellParticipantApproach(user)}});
      } else if (user.progressStatus === 100) {
        user.finishTime = Date.now();
        activeRoom.results.push(user);
        socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellParticipantFinished(activeRoom, user)}});
        socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.tellParticipantFinished(activeRoom, user)}});
      }

      socket.emit("GAME_EVENT", {name: "UPDATE_USERS_INFO", args: {users: activeRoom.users}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "UPDATE_USERS_INFO", args: {users: activeRoom.users}});
    }

    function onEndGame() {
      const filteredResults = activeRoom.users.sort(({progressStatus: a}, {progressStatus: b}) => b - a);
      for (let filteredResult of filteredResults) {
        if (!findUser(filteredResult.name, activeRoom.results)) {
          activeRoom.results.push(filteredResult);
          filteredResult.finishTime = Date.now();
        }
      }
      clearTimeout(timeoutDeleteCommentID);
      clearTimeout(timeoutRandomPhraseID);
      clearInterval(intervalID);


      socket.emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.announceResults(activeRoom)}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "COMMENT", args: {text: commentator.announceResults(activeRoom)}});
      socket.emit("GAME_EVENT", {name: "END_GAME_DONE", args: {results: activeRoom.results}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "END_GAME_DONE", args: {results: activeRoom.results}});
    }

    function onLeaveRoom() {
      activeRoom.usersCount--;
      const index = activeRoom.users.indexOf(findUser(username, activeRoom.users));
      activeRoom.users.splice(index, 1);

      socket.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: {rooms}});
      socket.broadcast.emit("GAME_EVENT", {name: "UPDATE_ROOMS", args: {rooms}});
      socket.to(activeRoom.name).emit("GAME_EVENT", {name: "RENDER_ROOM", args: {room: activeRoom}});

      socket.leave(activeRoom.name);
    }

    function onDisconnect() {
      console.log(username, socket.id, 'disconnected');
      users.delete(username);
    }
  });
};








