export function findRoom(room, rooms) {
    return rooms.find(({name}) => name === room);
}

export function findUser(username, users) {
    return users.find(({name}) => name === username);
}

