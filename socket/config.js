export const MAXIMUM_USERS_FOR_ONE_ROOM = 5;
export const SECONDS_TIMER_BEFORE_START_GAME = 10;
export const SECONDS_FOR_GAME = 60;
export const SECONDS_INTERVAL_FOR_GAME_COMMENT = 30;
export const SYMBOLS_LEFT_FOR_WARNING = 30;
export const SECONDS_TO_SHOW_COMMENT = 9;
export const SECONDS_BEFORE_RANDOM_PHRASE = 6;
