export const createRoomElement = ({name, usersCount}) => {
    const roomElement = `<div class="col-sm-3">
            <div class="card room">
              <div class="card-body">
                <h5 class="card-title">Room #${name}</h5>
                <p class="card-text">${usersCount} users connected</p>
                <button class="btn btn-primary join-btn">Join</button>
              </div>
            </div>
          </div>`;
    return roomElement;
}

export const createRoomsElement = (name, readyBtnText) => {
    const roomsElement = `<div class="container container-fluid ">
      <div id="left-panel">
        <h1>Room #${name}</h1>
        <button id="quit-room-btn" class="btn btn-primary">< Back to rooms</button>
        <ul id="users-list">
        </ul>
      </div>

      <div id="game-container" class="card">
        <div id="text-container" class="display-none"></div>
        <div id="timer" class="display-none"></div>
        <button id="ready-btn" class="btn btn-primary">${readyBtnText}</button>
      </div>
      <div id="message-container" class="message-container"></div>
    </div>`;
    return roomsElement;
}

export const createLiElement = (name, readyBtnClass, youLabel) => {
    const liElement = `<li>
            <div><i class="fas fa-circle ${readyBtnClass}"></i>${name} ${youLabel}</div>
            <div class="user-progress ${name}">
              <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" aria-valuenow=1" aria-valuemin="0" aria-valuemax="100" style="width: 0"></div>
              </div>
            </div>
          </li>`;
    return liElement;
}


export const createElement = ({ tagName, className, attributes = {} }) => {
    const element = document.createElement(tagName);

    if (className) {
        addClass(element, className);
    }

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
};

export const addClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.remove(...classNames);
};

export const getText = async () => {
    const response = await fetch(`/game/texts/`);
    const text = await response.text();
    return text;
}

export const formatClassNames = className => className.split(" ").filter(Boolean);

export function areReady(users) {
    let areReady = true;
    for (let {readyStatus} of users) {
        if (!readyStatus) {
            areReady = false;
        }
    }
    return areReady;
}

export function findUser(username, users) {
    return users.find(({name}) => name === username);
}