import {
  addClass, createElement, getText,
  removeClass, findUser, areReady, createRoomElement, createRoomsElement, createLiElement
} from "./helper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const createRoomBtn = document.getElementById('add-room-btn');
createRoomBtn.addEventListener("click", addRoom);

function addRoom() {
  const name = prompt('Enter room name', '');
  socket.emit("ADD_ROOM", name);
}

let gameTimeIntervalID;
let keyupListener;
let text;

socket.on("GAME_EVENT", gameEventListener)

function gameEventListener(event) {  //Facade
  const {name, args} = event;

  switch (name) {
    case "ADD_ROOM_DONE":
      onAddRoomDone(args);
      break;
    case "UPDATE_ROOMS":
      updateRooms(args);
      break;
    case "RENDER_ROOM":
      renderRoom(args);
      break;
    case "START_TIMER":
      startTimer(args);
      break;
    case "START_GAME":
      startGame(args);
      break;
    case "UPDATE_USERS_INFO":
      updateProgressBar(args);
      break;
    case "END_GAME_DONE":
      onEndGameDone(args);
      break;
    case "COMMENT":
      comment(args);
      break;
    case "DELETE_COMMENT":
      deleteComment(args);
      break;
    case "ERROR":
      handleError(args);
      break;
  }
}

function handleError({error, message}) {
  alert(message);
  if (error === 'user') {
    sessionStorage.removeItem('username');
    window.location.replace("/login");
  }
}

function onAddRoomDone({ room }) {
  socket.emit("JOIN_ROOM", room)
}

function updateRooms({ rooms }) {

  const roomsContainer = document.getElementById("rooms");
  roomsContainer.innerHTML = "";

  for (let room of rooms) {
    const roomElement = createRoomElement(room);

    roomsContainer.insertAdjacentHTML('beforeend', roomElement);
    const joinBtns = roomsContainer.querySelectorAll('.join-btn');

    for (let joinBtn of joinBtns) {
      joinBtn.addEventListener('click', () => {
        socket.emit("JOIN_ROOM", room);
      });
    }
  }
}


function renderRoom({ room }) {

    const roomsPage = document.getElementById('rooms-page');
    const gamePage = document.getElementById('game-page');
    const user = findUser(username, room.users);
    const readyBtnText = user.readyStatus ? 'Not ready' : 'Ready';

    const roomsElement = createRoomsElement(room.name, readyBtnText);

    gamePage.innerHTML = "";
    gamePage.insertAdjacentHTML('beforeend', roomsElement);

    const readyBtn = document.getElementById('ready-btn');
    const backToRoomsBtn = document.getElementById('quit-room-btn');

    const usersList = document.getElementById("users-list");
    for (let {name, readyStatus} of room.users) {
      const readyBtnClass = readyStatus ? 'ready-status-green' : 'ready-status-red';
      const youLabel = (name === username) ? '(you)' : '';
      const li = createLiElement(name, readyBtnClass, youLabel);

      usersList.insertAdjacentHTML('beforeend', li);
    }

    addClass(roomsPage, 'display-none');
    removeClass(gamePage, 'display-none');

    backToRoomsBtn.addEventListener('click', () => {
      addClass(gamePage, 'display-none');
      removeClass(roomsPage, 'display-none');

      socket.emit("LEAVE_ROOM");
    });

    readyBtn.addEventListener('click', () => {
      user.readyStatus = !user.readyStatus;

      socket.emit("UPDATE_ROOM", user.readyStatus);
      if (areReady(room.users)) {
        socket.emit("USERS_READY");
      }
    });
}

async function startTimer({ timeBeforeGame }) {

  const timer = document.getElementById('timer');
  const quitBtn = document.getElementById('quit-room-btn');
  const readyBtn = document.getElementById('ready-btn');

  readyBtn.style.display = "none";
  quitBtn.style.display = "none";

  removeClass(timer, 'display-none');
  timer.innerHTML = `${timeBeforeGame}`;

  const timerIntervalID = setInterval(() => {
    timer.innerHTML = `${--timeBeforeGame}`;
    if (timeBeforeGame === 0) {

      socket.emit("TIMER_DONE", text.length);
      clearInterval(timerIntervalID);
    }
  }, 1000);

  text = await getText();
}

function startGame({ timeLeft }) {
  const timer = document.getElementById('timer');
  const gameContainer = document.getElementById('game-container');
  const textContainer = document.getElementById('text-container');
  const time = createElement({tagName: 'div', className: 'time-left-label'});
  const selectedText = createElement({tagName: 'span', className: 'selected-text'});
  const leftText = createElement({tagName: 'span', className: 'left-text'});

  textContainer.innerHTML = '';

  removeClass(textContainer, 'display-none');
  addClass(timer, 'display-none');

  selectedText.innerHTML = '';
  leftText.innerHTML = text;
  textContainer.append(selectedText, leftText);
  gameContainer.prepend(time);
  time.innerHTML = `${timeLeft} seconds left`;


  gameTimeIntervalID = setInterval(() => {
    time.innerHTML = `${--timeLeft} seconds left`;

    if (timeLeft === 0) {
      socket.emit("END_GAME");
      clearInterval(gameTimeIntervalID);
    }
  }, 1000);

  keyupListener = (event) => {
    if (event.key === leftText.innerHTML[0]) {

      selectedText.innerHTML = selectedText.innerHTML + event.key;
      leftText.innerHTML = leftText.innerHTML.slice(1, leftText.innerHTML.length + 1);

      const typedText = selectedText.innerHTML.length;
      socket.emit("UPDATE_PROGRESS", typedText);
    }
  }

  document.addEventListener('keyup', keyupListener);
}



function updateProgressBar({ users }) {
  let allFinished = true;
  for (let user of users) {
    const progressBar = document.querySelector(`.user-progress.${user.name} .progress-bar`);
    progressBar.ariaValuenow = `${user.progressStatus}`;
    progressBar.style.width = `${user.progressStatus}%`;
    if (user.progressStatus === 100) {
      removeClass(progressBar, 'bg-warning');
      addClass(progressBar, 'bg-success');
    } else {
      allFinished = false;
    }
  }

  if (allFinished) {
    socket.emit("END_GAME");
    clearInterval(gameTimeIntervalID);
  }
}

function comment({ text = '' }) {
  updateMessageContainer(text);
}

function deleteComment() {
  updateMessageContainer();
}

function updateMessageContainer(text) { //Factory
  const messageContainer = document.getElementById('message-container');
  messageContainer.innerHTML = '';

  if (text) {
    const messageElement = `<div class="message"><p class="message-content">${text}</p></div>`;
    messageContainer.insertAdjacentHTML('beforeend', messageElement);
  }
}

function onEndGameDone() {
  if (gameTimeIntervalID) {
    clearInterval(gameTimeIntervalID);
  }

  document.removeEventListener('keyup', keyupListener);
}
